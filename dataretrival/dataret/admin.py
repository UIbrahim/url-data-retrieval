from django.contrib import admin
from .models import Dataret


class DataretAdmin(admin.ModelAdmin):
    list_display = '__all__'


admin.site.register(Dataret)
