from rest_framework import serializers
from .models import Dataret


class DataretSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataret
        fields = '__all__'