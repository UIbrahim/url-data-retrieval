from django.db import models


class Dataret(models.Model):
    title = models.CharField(max_length=100, null=True)
    central_text = models.CharField(max_length=2083, null= True)
    url = models.URLField(max_length=2083)
    image_url = models.URLField(max_length=2083, null= True)

    def __str__(self):
        return self.title
