from django.shortcuts import render
from rest_framework import viewsets
from .models import Dataret
from .serializers import DataretSerializer
from .forms import DataretForm
from .scraping import dataretr


def index(request):
    form = DataretForm()
    if request.method == 'POST':
        form = DataretForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data.get("url")
            dataretr(url)
            record = Dataret(title=dataretr.title, image_url=dataretr.image, url=url, central_text=dataretr.text)
            record.save()

    context = {'form': form,}
    return render(request, 'dataret/index.html', context)



class DataretView(viewsets.ModelViewSet):
    queryset = Dataret.objects.all()
    serializer_class = DataretSerializer