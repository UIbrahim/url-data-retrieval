from django.forms import ModelForm
from .models import Dataret


class DataretForm(ModelForm):
    class Meta:
        model = Dataret
        fields = ('url',)
