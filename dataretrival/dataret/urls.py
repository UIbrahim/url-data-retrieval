from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('Dataret', views.DataretView)
urlpatterns = [
    path('', views.index),
    path('api', include(router.urls))
]