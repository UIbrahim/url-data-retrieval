from django.apps import AppConfig


class DataretConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dataret'
