import bs4
import requests


def dataretr(url):
    result = requests.get(url)
    soup = bs4.BeautifulSoup(result.text, 'lxml')

    title = soup.select('title')
    img_url = soup.select('img')
    central_text = soup.select('article')
    text = ''
    for word in central_text:
        text += word.text

    dataretr.title = title[0].getText()
    dataretr.image = img_url[0]['src']
    dataretr.text = text

